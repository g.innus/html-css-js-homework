function togglePreview(btn, targeId) {
    var article = document.getElementById(targeId);

    if (article.classList.contains("preview")) {
        article.classList.remove("preview");
        btn.innerHTML = "Colapse";
        btn.scrollIntoView();
    } else {
        article.classList.add("preview");
        btn.innerHTML = "Expand";
    }

}